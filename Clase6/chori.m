function b = chori(a);
    [fil, col, cap] = size(a);
    if cap == 1, b =a; return; end
    b = reshape(a,[fil,col*cap]);
end
