%Parte 1
%{
clear all; close all; clc
a = imread('paisaje.jpg'); figure; imshow(a);
b = rgb2gray(a); figure(2); imshow(b);
c = 255 - a; d = 255 - b;
figure(3); imshow([a,c]);
figure(4); imshow([b,d]);
%}

%Parte 2
%{
clear all; close all; clc
a = imread('paisaje.jpg'); [fil, col, cap] = size(a);
b = imread('car.png'); b = imresize(b, [fil, col]); b = b(:,:,1:3);
c = a + b; figure(1); imshow([a, b, c]); impixelinfo
d = (a*0.5)+(b*0.5); figure(2); imshow([a, b, d]); impixelinfo
for i = 0 : .01 : 1
    e = a * i + b * (1-i);
    figure(3); imshow(e);
    pause(0.01);
end
for i = 1 : -.01 : 0
    e = a * i + b * (1-i);
    figure(3); imshow(e);
    pause(0.01);
end
%}

%Parte 3
clear all; close all; clc;
a = imread('paisaje.jpg'); [fil, col, cap] = size(a);
b = imread('car.png'); b = imresize(b, [fil, col]); b = b(:,:,1:3);
ind = find(b>0);
a(ind) = b(ind);
figure(1); imshow(a); impixelinfo
for i = 1 : -5 : 255
   c = a + i;
   figure(3); imshow(c); impixelinfo
   pause(0.1)
end