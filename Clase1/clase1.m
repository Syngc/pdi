clear all
close all
clc
a = imread('imagen.png');
figure(1)
imshow(a);

r = a;
g = a;
b = a;
r(:,:,2:3) = 0;
figure(2)
imshow(r);

g(:,:,1:2:3) = 0;
figure(3)
imshow(g)

b(:,:, 1:2) = 0;
figure(4)
imshow(b)

bw = rgb2gray(a);
figure(5)
imshow(bw)

c = [bw,bw,bw];
figure(6)
imshow(c)

[fil,col] = size(bw);
c1 = reshape(c,[fil,col,3]);
figure(7)
imshow(c1)
